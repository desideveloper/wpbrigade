<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <meta name="p:domain_verify" content="7dccd02ce1119b95fc8ad56650a588f7"/>
  <link rel="shortcut icon" type="image/ico" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.ico"/>
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png"/>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <link href='https://fonts.googleapis.com/css?family=Muli:800,900' rel='stylesheet' type='text/css'>
  <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
	<?php wp_head(); ?>

      <!-- Facebook Pixel Code -->
    <script>

    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1617396648507586');
    fbq('track', "PageView");
    fbq('track', 'ViewContent');
    fbq('track', 'Lead');

    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1617396648507586&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>
  <!-- #masthead -->
    <!-- Navigation -->
	<?php if( !is_page_template( 'page-no-header-footer.php' )) : ?>
    <div class="mainMontainer">
    <nav id="sidebar-wrapper">
      <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'container_class' => 'sidebar-nav', 'menu_class' => 'sidebar-nav' ) ); ?>
    </nav>

    <span id="menu-toggle"></span>
    <!-- Header -->
    <header class="inner_header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <a  href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo">
            	<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="385" height="131" alt="Wordpress Brigade">
            </a>
            <p class="cations">
              A Growing Brigade of WordPress Developers.
            </p>
          </div>
        </div>
        <div id="army"></div>
        <!-- /.row -->
      </div>
      <div class="cloud1"></div>
      <div class="cloud2"></div>
      <div class="cloud3"></div>
      <div class="ballon"></div>
      <div class="plane"></div>
    </header>
	<div id="main" class="site-main">
  <div class="container">
        <div class="row">
		<?php endif; ?>
