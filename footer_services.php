<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php if( !is_page_template( 'page-no-header-footer.php' )) : ?>
		</div><!-- #content_main -->
		</div><!-- #content_main -->
		</div><!-- #main -->
    <?php endif; ?>
		<footer class="footer services_footer">
      <div class="container">
        <div class="row footer_main_section">
        	<div class="col-lg-3 col-md-3 col-sm-3 text-center">
          	<h3>WPBrigade</h3>
          	<p class="footer_light our_info">We are a bunch of Software developers who are shaping your ideas to reality on Internet. We felt in love with WordPress since 2007 and building themes and plugins since then.</p>
            <br />
            <a href="https://www.facebook.com/pages/WPBrigade/519247374844434"  class="genericon genericon-facebook-alt"></a>
            <a href="http://twitter.com/wpbrigade"  class="genericon genericon-twitter"></a>
            <a href="https://plus.google.com/103941758270176955181" class="genericon genericon-googleplus-alt" rel="publisher" ></a>
            <a href="http://www.pinterest.com/wbrigade/" class="genericon genericon-pinterest"></a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 text-center">
          	<h3>Our services</h3>
          	<ul class="footer_list">
              <li><a href="/services/">PSD to WordPress</a></li>
              <li><a href="/services/">PSD to HTML/CSS</a></li>
              <li><a href="/services/">PSD to Email Templates</a></li>
              <li><a href="/services/">Custom WordPress Themes</a></li>
              <li><a href="/services/">WordPress Theme Customization</a></li>
              <li><a href="/services/">Hubspot Landing Pages</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 text-center">
          	<h3>Quick links</h3>
          	<ul class="footer_list">
              <li><a href="/contact/">Contact</a></li>
              <li><a href="/services/">Services</a></li>
              <li><a href="/blog/">Blog</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 text-center">
          	<h3>Talk to us</h3>
          	<p class="footer_light">Tweet Us at</p>
            <a href="http://twitter.com/wpbrigade" target="_blank" title="Tweet us @WPBrigade" class="link_blue">@WPBrigade</a>
            <br /><br />
            <!-- <p><span class="footer_light">Or check our</span> <br />
<a href="/forums/" class="link_blue" title="Support/forums">Support/forums</a></p> -->
          </div>
        </div>
      </div>
			<div class="Copyright_section">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 footer_light text-center">
            Innovating with
            <img src="<?php echo get_template_directory_uri(); ?>/img/wordpress.png" alt="WordPress">
            <a href="https://wordpress.org/" target="_blank" rel="nofollow">WordPress</a> since 2007
            <br>© Copyright 2017 WPBrigade | A WordPress Development Agency
            <!-- <img src="<?php //echo get_template_directory_uri(); ?>/img/wpengine2.png" alt="WPEngine">
            <img src="<?php //echo get_template_directory_uri(); ?>/img/maxcdn.png" alt="MaxCDN"> -->
					</div>
      	</div>
      </div>
    	</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->
	
  <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
	<?php wp_footer(); ?>
</body>
</html>
