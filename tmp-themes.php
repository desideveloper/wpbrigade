<?php
/**
 * Template Name: Themes Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>
        <section class="plugins_title">
            <div class="container_wpb">
                <h1>Best Themes Build by Team WPBrigade</h4>
                <p class="h3">We have crafted these innovative WordPress themes so far and more are in the process</p>
            </div>
        </section>
        <section class="plugins_list theme_list">
            <div class="container_wpb">
                <div class="badges_board">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/bloggers.png" alt="">
                    <span class="top_bar"></span>
                </div>
                <div class="plugin_detials">
                    <h3>Bloggerz</h3>
                    <p>Bloggerz is easy and joyfull theme for blogging. Bloggerz make some usefull feature in Customizer which can allow you to easily set your theme.</p>
                    <a href="https://wpbrigade.com/demos/bloggerz/" class="btn blue_btn">Demo</a>
                    <!-- <a href="https://downloads.wordpress.org/theme/bloggerz.latest-stable.zip" class="btn blue_btn">Download</a> -->
                    <?php echo do_shortcode('[purchase_link id="1536" text="Download" style="button" class="btn blue_btn"]');?>
                </div>
            </div>
        </section>

        <section class="hire_us_section" style="background-color: #fefefe">
            <div class="container_wpb">
                <h4 class="h2">Do you need The Best WordPress development services?</h4>
                <p>You are at the right place. Hire the Best WordPress Developers, Front-end Developers, CMS Development, Support and Maintenance.</p>
                <a href="#" class="btn blue_btn hire_btn">Get a free Quote</a>
            </div>
        </section>

<?php
//get_sidebar();
get_footer();
?>