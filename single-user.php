<?php

/**
 * Single User
 *
 * @package bbPress
 * @subpackage Theme
 */

get_header("new"); ?>
<div  class="col-lg-8 col-lg-offset-2">
	<?php do_action( 'bbp_before_main_content' ); ?>

	<div id="bbp-user-<?php bbp_current_user_id(); ?>" class="bbp-single-user">
		<div class="entry-content">

			<?php bbp_get_template_part( 'content', 'single-user' ); ?>

		</div><!-- .entry-content -->
	</div><!-- #bbp-user-<?php bbp_current_user_id(); ?> -->

	<?php do_action( 'bbp_after_main_content' ); ?>
</div>
<?php //get_sidebar('forums'); ?>
<?php get_footer(); ?>
