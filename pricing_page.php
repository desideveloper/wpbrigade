<?php
/**
 * Template Name: Pricing page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>

<div class="plugin_top">
	<div class="container_wpb slid">
		<div class="left_sec">
			
			<h2>LoginPress</h2>
			<h3>The Best Login Customizer plugin for WordPress</h3>
			<div class="live-demo">
				<a href="#" class="d_free">Download Free Version</a>
				<p>Live demo</p>

				<a href="#" class="buy_n">Upgrad to Premium</a>
				<p>Buy Now</p>
				
			</div>
			
		</div>
		<div class="right_sec">
			<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
			<img class="mobile_" src="<?php echo get_template_directory_uri(); ?>/img/mobile.png">
		</div>
	</div>
</div>
<div class="achivement_lists plugin_achive">
    <div class="container_wpb">
        <ul>
            <li>
            	<p>Downloads</p>
                <h5>10k</h5>
            </li>
            <li>
            	<p>Version</p>
                <h5>1.1</h5>
            </li>
            <li>
            	<p>Plugin</p>
                <h5>10</h5>
            </li>
        </ul>
    </div>
</div>
<div class="plugin_main container_wpb clearfix">
	
	<div class="plugin_feature login_pr clearfix">
		<h4>Feature</h4>
		<div class="feature ad_on">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_1.png">
			</div>
			<h3>Pre-Designed login templates</h3>
			<p>Our Pre-designed templates are just one click away. You can use these to wow! your login screens.</p>
		</div>

		<div class="feature ">
			<div class="feature_icon">	
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_2.png">
			</div>
			<h3>Custome login error messages</h3>
			<p>You can change every error message wether it is on login form, forgot password form or register form.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_3.png">
			</div>
			<h3>Your logo on login screen</h3>
			<p>You can easily change your logo image, update title text, add links, customize the sizing and spaces.</p>
		</div>

		<div class="feature ">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_4.png">
			</div>
			<h3>login page background</h3>
			<p>Edit login page background, add your favorite images at the background. You can add plan colors as well.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_5.png">
			</div>
			<h3>login form style</h3>
			<p>You can style login form fields, buttons, labels and colors. Almost everything in a form.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_6.png">
			</div>
			<h3>Add login page footer</h3>
			<p>We call login page footer area under the login form fields</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_5.png">
			</div>
			<h3>Custom CSS</h3>
			<p>Custom CSS area which can Customize login page in more advanced way. Recommended for advanced users.</p>
		</div>

		<div class="feature popular_on">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_6.png">
			</div>
			<h3>Custom JS</h3>
			<p>Customize your login page in more advanced way using our Custom JS area. Add your JS there and make your login page more advanced.</p>
		</div>
		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_5.png">
			</div>
			<h3>Google reCAPTCHA</h3>
			<p>Introducing the new reCAPTCHA feature. Protect your website from spam and abuse while letting real people pass through with ease.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_6.png">
			</div>
			<h3>Google Fonts</h3>
			<p>Now you can make your login page WOW! with google fonts.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_5.png">
			</div>
			<h3>More features are coming</h3>
			<p>We are working hard so we can provide unlimited features in loginpress.</p>
		</div>


	</div>

</div>
<div>
	<div class="screen_shots">
	<h2>Screenshots</h2>
	<div class="screen_s">
		<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		<h3>Template Name</h3>
	</div>
	<div class="screen_s">
		<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		<h3>Template Name</h3>
	</div>
	<div class="screen_s">
		<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		<h3>Template Name</h3>
	</div>
	<div class="screen_s">
		<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		<h3>Template Name</h3>
	</div>
	<div class="screen_s">
		<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		<h3>Template Name</h3>
	</div>		
			
	<a href="#">See More</a>
			

	</div>

	<div class="container_wpb">
		<div class="pricing_table clearfix">
			<h5>Pricing</h5>
			<div class="price_1">
				<h2>Free</h2>
				<h3>$0</h3>
				<a href="#">Buy Now</a>
				<ul>
					<li>1 Website</li>
				</ul>
			</div>

			<div class="price_1">
				<h2>Personal</h2>
				<h3>$29</h3>
				<a href="#">Buy Now</a>
				<ul>
					<li>1 Website</li>
					<li>1 Year Support</li>
					<li>1 Year of Updates</li>
				</ul>
			</div>

			<div class="price_1">
				<h2>Small Business</h2>
				<h3>$59</h3>
				<a href="#">Buy Now</a>
				<ul>					
					<li>10 Website</li>
					<li>1 Year Support</li>
					<li>1 Year of Updates</li>
					<li>Priority Support</li>
				</ul>
			</div>

			<div class="price_1">
				<h2>Agency</h2>
				<h3>$99</h3>
				<a href="#">Buy Now</a>
				<ul>					
					<li>25 Website</li>
					<li>1 Year Support</li>
					<li>1 Year of Updates</li>
					<li>Priority Support</li>
				</ul>
			</div>
			
		</div>
		<div class="pricing_bot"> 
				<p>OR <br>Get the developer license (<a href="#">unlimited sites license - $199</a>)</p>
			</div>
	</div>



	<div class="plugin_testimonial">
		<h2>Testimonial</h2>
		<p class="some">some of our following comments.</p>
		<div class="container_wpb comme">
			<div class="comments_section right">
			    
			    <div class="comment">
			        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor.</p>
			    </div>
			    <div class="user_d">
			        <div class="user_d_img">
			            <img src="<?php echo get_template_directory_uri(); ?>/img/services-1.png">
			        </div>
			        
			        <div class="user_inf">
			            <p>Zaryab Ahmed</p>
			            <a href="#">Zaryabkhan425@gmail.com</a>
			        </div>
			    </div>
			</div>

			<div class="comments_section left">
			    <div class="comment">
			        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor.</p>
			    </div>
			    <div class="user_d">
			        <div class="user_d_img">
			            <img src="<?php echo get_template_directory_uri(); ?>/img/services-1.png">
			        </div>
			        
			        <div class="user_inf">
			            <p>Zaryab Ahmed</p>
			            <a href="#">Zaryabkhan425@gmail.com</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>


</div>
<div class="faq">
	<div class="container_wpb">
		<h2>Frequently Asked Questions</h2>
		<div class="left">
			<h3>How to get started with LoginPress?</h3>
			<p>In your WordPress Dashboard, navigate to Appearance > LoginPress to get started.</p>

			<h3>Who should use LoginPress?</h3>
			<p>Every WordPress user should use LoginPress. It is easy to use and can tranform your boring wp-login.php login page into a beautiful customized login form page.</p>

			<h3>Is LoginPress translation ready?</h3>
			<p>Yes, LoginPress has full translation and localization support via the loginpress textdomain. All .mo and .po translation files should go into the languages folder in the base of the plugin.</p>

			<h3>How to Install or Use LoginPress Pro?</h3>
			<p>Step-by-step instructions on How to Upgrade from existing Free version to Pro </p>
			<p>1. You have installed and setup Free version already.<br>
			2. Upload the Pro version.<br>
			3. Pro features will be enabled automatically.<br>
			4. You don't need to setup Free version options again.<br>
			5. Setup Pro features like Google fonts, Google reCaptcha, Choose themes etc</p>
		</div>
		<div class="right">
			<h3>Do I Need LoginPress Pro?</h3>
			<p>If you have a WordPress Website, and you want to add a cool looking login page. LoginPress is your must have plugin.</p>

			<h3>How to upgrade to LoginPress Pro?</h3>
			<p>LoginPress Pro is a premium plugin which works if you have installed Free version already. So, first install our Free version and then install the Pro package. <br>LoginPress Pro version extends the Premium functionality to our Core Free version of LoginPress.</p>

			<h3>Where is my license key?</h3>
			<p>License key is the Order ID which looks like in this format. <br>
			WHI150807-1234-12345<br>
			You can find it in the email Sales Receipt.</p>

			<h3>Do I need to have coding skills to use LoginPress?</h3>
			<p>No, It is very easy to setup. Just plug and play. Have fun!</p>

			<h3>Is LoginPress WPML Compatible?</h3>
			<p>Yes</p>
		</div>
		<p class="hesitate">Do you have any question ? don't hesitate to <a href="#">Contact Us</a></p>
	</div>
</div>









<?php
//get_sidebar();
get_footer();
?>