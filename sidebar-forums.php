<?php
/**
 * The Sidebar containing the forum widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div class="col-lg-3 col-md-3">

	<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
	<div id="forum-sidebar" class="forum-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- #forum-sidebar -->
	<?php endif; ?>
</div><!-- #forum -->
