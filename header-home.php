<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <meta name="p:domain_verify" content="7dccd02ce1119b95fc8ad56650a588f7"/>
  <link rel="shortcut icon" type="image/ico" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.ico"/>
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png"/>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <link href='https://fonts.googleapis.com/css?family=Muli:300,400,500,600,800,900' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
	<?php wp_head(); ?>

      <!-- Facebook Pixel Code -->
    <script>

    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1617396648507586');
    fbq('track', "PageView");
    fbq('track', 'ViewContent');
    fbq('track', 'Lead');

    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1617396648507586&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>
  <!-- #masthead -->
    <!-- Navigation -->
	<?php if( !is_page_template( 'page-no-header-footer.php' )) : ?>
 


        <header class="header">
            <div class="container_wpb">
                <a href="/" class="logo_2">WPBrigade</a>
                <a href="#" class="btn blue_btn hire_btn fl_left">Get a free Quote</a>
                <div id="nav-icon2">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
                <div class="nav">


                    <!-- <ul>
                        <li><a href="#">Plugins</a>
                          <ul>
                            <li><a href="#">LoginPress</a></li>
                            <li><a href="#">Analytify</a></li>
                            <li><a href="#">Related Posts</a></li>
                          </ul>
                        </li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Our Work</a></li>
                        <li><a href="#">Tutorials</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul> -->
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'container_class' => '', 'menu_class' => '' ) ); ?>
                </div>
            </div>
        </header>
        <div class="hire_popup_wraper hide_hire_popup">
          <div class="hire_popup">
            <div class="hire_popup_inner">
              <h3>Get a free Quote</h3>
              <?php echo do_shortcode( '[contact-form-7 id="1321" title="hire us form"]' ); ?>
              <span class="close_btn"></span>
            </div>
          </div>
        </div>
	 
		<?php endif; ?>
