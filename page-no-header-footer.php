<?php

/**
 * Template Name: Elementor - No Container | No Header, No Footer
 *
 * @package Elementor
 * @subpackage Theme
 */

get_header("blog"); ?>
<div id="main-content">
	<div id="primary" class="site-content" role="main">
		<?php the_content( $more_link_text = null, $strip_teaser = false ) ?>
	</div>
</div>
<?php get_footer();
