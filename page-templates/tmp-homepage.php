<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>

        <section class="banner_sec">
            <div class="main_heading">
                <h1>One stop for your WordPress development needs</h1>
                <h2>Premium PSD to WordPress Development Agency</h2>
                <a href="#nogo" class="btn white_btn hire_btn">Get a free Quote</a>
            </div>
            <div class="container_wpb">
                <div class="badges_board">
                    <h3>Team WPBrigade have acheived these badges.</h3>
                    <div class="wp_badges_list">
                        <a href="https://analytify.io/analytify-is-proud-to-be-a-google-analytics-technology-partner/" target="_blank" class="google_partner"><span></span>Google Technology Partner</a>
                        <a href="https://github.com/phpmyadmin/phpmyadmin/commits?author=hiddenpearls" target="_blank" class="phpmyadmin"><span></span>phpMyAdmin Contributor</a>
                    </div>
                    <ul class="wp_badges_list wp_badges">
                        <li>
                            <a href="https://jetpack.pro/profile/hiddenpearls/" target="_blank">
                                <span></span>
                                Core Contributor
                            </a>
                        </li>
                        <li>
                            <a href="https://jetpack.pro/profile/hiddenpearls/" target="_blank">
                                <span></span>
                                Translation Contributor
                            </a>
                        </li>
                        <li>
                            <a href="https://jetpack.pro/profile/hiddenpearls/" target="_blank">
                                <span></span>
                                Theme Developer
                            </a>
                        </li>
                        <li>
                            <a href="https://jetpack.pro/profile/hiddenpearls/" target="_blank">
                                <span></span>
                                Plugin Developer
                            </a>
                        </li>
                        <li>
                            <a href="https://jetpack.pro/profile/hiddenpearls/" target="_blank">
                                <span></span>
                                Theme Review Team
                            </a>
                        </li>
                    </ul>
                    <span class="top_bar"></span>
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container_wpb">
                <h2>Our Premium Services</h2>
                <p>From the beginning, We are offering a borad range of custom WordPress development services.</p>
                <div>
                    <div class="offer">
                        <span></span>
                        <h4>Custom WordPress Themes</h4>
                        <p>We do Semantic, Validated, Responsive, SEO optimized and Retina ready themes</p>
                    </div> <!--  .offer -->
                    <div class="offer">
                        <span></span>
                        <h4>PSD to WordPress</h4>
                        <p>Get convert Your highly quality designs in Responsive WordPress Sites</p>
                    </div> <!--  .offer -->
                    <div class="offer">
                        <span></span>
                        <h4>PSD to HTML/CSS</h4>
                        <p>Responsive, Semantic, SEO optimized and w3c Compatible your designs</p>
                    </div> <!--  .offer -->
                    <div class="offer">
                        <span></span>
                        <h4>Ongoing WordPress Maintenance</h4>
                        <p>We can help with your WordPress sites running.</p>
                    </div> <!--  .offer -->
                </div> <!--  .offers -->
            </div>
        </section>
        <section class="plugin_section">
            <div class="container_wpb">
                <h2>Plugins We Have!</h2>
                <p>We have crafted these innovative WordPress products so far and more are in the process</p>
                <div class="plugins">
                    <a href="/wordpress/plugins/related-posts/" class="plugin">
                        <div>
                        <div class="image related_post">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/related-posts.png" alt="">
                        </div> <!--  .image -->
                        <div class="caption">
                            <h5>Related Posts Thumbnails</h5>
                            <p>Related Posts is The Best Customizable plugin, that nicely displays related posts thumbnails under the post. Which increases user engagement with your blog.</p>
                            <ul class="stats">
                                <li class="icon-user">30,000+</li>
                                <li class="icon-downld">279,143</li>
                                <li class="icon-eye">4.9</li>
                            </ul> <!--  .stats -->
                        </div> <!--  .caption -->
                        </div>
                    </a> <!--  .plugin -->
                    <a href="http://wpbrigade.com/recommend/analytify" target="_blank" class="plugin">
                        <div>
                        <div class="image analitify_a">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/analytify.png" alt="">
                        </div> <!--  .image -->
                        <div class="caption">
                            <h5>Google Analytics for WordPress</h5>
                            <p>Analytify is the must-have Plugin for Google Analytics in WordPress. It helps in tracking and reporting Google analytics events within WordPress.</p>
                            <ul class="stats">
                                <li class="icon-user">9,000+</li>
                                <li class="icon-downld">75,554</li>
                                <li class="icon-eye">4.9</li>
                            </ul> <!--  .stats -->
                        </div> <!--  .caption -->
                        </div>
                    </a> <!--  .plugin -->
                    <a href="/wordpress/plugins/loginpress/" class="plugin">
                        <div>
                        <div class="image login_press">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/login_press.png" alt="">
                        </div> <!--  .image -->
                        <div class="caption">
                            <h5>Login Page Customizer</h5>
                            <p>LoginPress plugin allows you to easily customize the layout of login, admin login, client login, register and forgot password forms/pages.</p>
                            <ul class="stats">
                                <li class="icon-user">5,000+</li>
                                <li class="icon-downld">28,027</li>
                                <li class="icon-eye">5.0</li>
                            </ul> <!--  .stats -->
                        </div> <!--  .caption -->
                        </div>
                    </a> <!--  .plugin -->
                </div> <!--  .plugins -->        
            </div> <!--  .container -->
        </section>
        <section class="testimonial_section">
            <div class="container_wpb">
                <h4 class="h2">Testimonials</h4>
                <p>Following are some of the 5-star reviews we got</p>

                <div class="testimonials">
                    <div class="testimonial_comment">
                        <p class="rating_title">LoginPress offers a complete password-screen customization solution</p>
                        <p>The authors offer responsive support and clearly have made a long-term commitment to ongoing development. Requests for assistance receive immediate attention.</p>
                    </div>
                    <div class="testimonial_by">
                        <span class="avator sam">
                            
                        </span>
                        <div class="testimonial_by_name">
                            <span class="name_t">@bpc</span>
                            <span class="name_a">wordpress.org user</span>
                        </div>
                    </div>
                </div>
                <div class="testimonials">
                    <div class="testimonial_comment">
                        <p class="rating_title">Works right out of the box.</p>
                        <p>Slick,quick and easy, recommended</p>
                    </div>
                    <div class="testimonial_by">
                        <span class="avator sam">
                            
                        </span>
                        <div class="testimonial_by_name">
                            <span class="name_t">@eyorks</span>
                            <span class="name_a">wordpress.org user</span>
                        </div>
                    </div>
                </div>
                <div class="testimonials">
                    <div class="testimonial_comment">
                        <p class="rating_title">Sweet!</p>
                        <p>LoginPress helped me to easily style my login-page from the ugly grey to a more “branded” login – really fast and the support is great!</p>
                    </div>
                    <div class="testimonial_by">
                        <span class="avator sam">
                            
                        </span>
                        <div class="testimonial_by_name">
                            <span class="name_t">@ordjar</span>
                            <span class="name_a">wordpress.org user</span>
                        </div>
                    </div>
                </div>
                <div class="testimonials">
                    <div class="testimonial_comment">
                        <p class="rating_title">If you aren’t using this, you are missing out!</p>
                        <p>I have nothing bad to say about Analytify. It is the easiest plugin to not only install and connect, but to read. Their WP dashboard makes Google Analytics easy to read and understand.</p>
                    </div>
                    <div class="testimonial_by">
                        <span class="avator sam">
                            
                        </span>
                        <div class="testimonial_by_name">
                            <span class="name_t">@jenily</span>
                            <span class="name_a">wordpress.org user</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="hire_us_section">
            <div class="container_wpb">
                <h4 class="h2">Do you need The Best WordPress development services?</h4>
                <p>You are at the right place. Hire the Best WordPress Developers, Front-end Developmenter, CMS Development, Support and Maintenance</p>
                <a href="#" class="btn blue_btn hire_btn">Get a free Quote</a>
            </div>
        </section>

<?php
//get_sidebar();
get_footer();
?>