<?php
/**
 * Template Name: order Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/order-styles.css">
	<section class="services_title">
        <div class="container_wpb heading-p">
            <h1>Let's Talk Further</h4>
            <p>Fill this form out so we have a basic understanding of your needs. We typically respond within 2-3 hours.</p>
        </div>
    </section>
    <section class="order">
        <div class="container_order">

            <div class="row auto_counter active">
            <div class="p_type">
        		<h3>Select your project type?</h3>
        	</div>
                <div class="col-md-4 col-sm-6">
                    <div class="order-box tick_mark service-box" data-target="PSDtoHTMLCSS">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_html_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>PSD to HTML/CSS</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="order-box  service-box" data-target="PSDtoEmail">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_email_template_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>PSD to Email</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="order-box  service-box"  data-target="PSDtoWordPress">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_wordpress_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>PSD to WordPress</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="order-box  service-box" data-target="WordPressCustomization">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/wordpress_customization_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>WordPress Customization</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="order-box  service-box" data-target="Hubspot">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/hubspot_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>Hubspot</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="order-box  service-box"  data-target="Deconetwork">
                        <div class="service-img-box">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                                >
                            <g id="XMLID_1_">
                                <g id="XMLID_2_">
                                    <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                    <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                    <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                    <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                    <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                    <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                    <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                    <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                    <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                                </g>
                                <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                    c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                    C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                            </g>
                            </svg>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/deco_network_icon.svg" alt="">
                        </div> <!--  .service-img-box -->
                        <h4>Deconetwork</h4>
                    </div> <!--  .order-box service-box -->
                </div> <!--  .col-md-4 -->
            </div> <!--  .row -->
						<!-- <div>
							<div class="order_form auto_counter">
	            	<div class="bud">
	            		<h3>What is your budget?</h3>
	            	</div>

	            	<div class="bud_section">
	            		 	<input type="text" placeholder="100$ - 500$"><span>-</span>
	            		 	<input type="text" placeholder="500$ - 1000$"><span>-</span>
	            		 	<input type="text" placeholder="1000$ - 5000$"><span>-</span>
	            		 	<input type="text" placeholder="Other" class="adju">
	            	</div>
	            </div>
							<div class="order_form auto_counter">
	            	<div class="project-details">
	            		<h3>Give us detail about you and your project?</h3>
	            	</div>
	            	<div class="p_d_form">
	            		 	<input type="text" class="left" placeholder="Zaryab Ahmed">
	            		 	<input type="text" class="right" placeholder="Enter Your Email Address">
	            		 	<input type="text" class="left" placeholder="Enter Your Phone Number">
	            		 	<input type="text" class="right upload_file" placeholder="Upload Your File">
	            		 	<a href="#" class="upload_btn"><img src="<?php echo get_template_directory_uri(); ?>/img/upload.png"></a>
	            		 	<textarea placeholder="Enter Detail About Your Project"></textarea>
	            		 	<input type="submit" value="Get a Quote" class="submit_b">
	            	</div>
	            </div>
						</div> -->
						<div class="form-container">
							<?php echo do_shortcode( '[contact-form-7 id="1289" title="Contact form 1"]', $ignore_html = false ) ?>
						</div>
        </div>
    </section>

<?php
//get_sidebar();
get_footer();
?>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$('<span class="upload_btn"><img src="<?php echo get_template_directory_uri(); ?>/img/upload.png"></span>').insertAfter('.file-name');

			$('.form-container .p_d_form  div.file-up-container .file input').change(function (){
				var fileName = $(this).val();
				if(fileName.length >0){
					$(this).parents().find('.file-name').html(fileName).css('color', "#3c4555" );
				}
				else{
					$(this).parents().find('.file-name').html("Uplad your file").css('color', "#939393" );
				}
			});

			$('input[name="PSDtoHTMLCSS[]"]').attr('checked' , true);
			$('.service-box').on('click', function(){
				var target=$(this).data('target');

				if($(this).hasClass('tick_mark')){
					$(this).removeClass('tick_mark');
					$('.checkboxes .'+target+' input').attr('checked' , false);
				}
				else{
					$(this).addClass('tick_mark');
					$('.checkboxes .'+target+' input').attr('checked' , true);
				}
			});
		});
	})(jQuery)
</script>
