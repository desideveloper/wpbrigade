<?php
/**
 * Template Name: Services Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("services"); ?>

        <section class="services_title">
            <div class="container_wpb heading-p">
                <h1>Our Services</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
            </div>
        </section>
        <div class="services_list container_wpb">
            <div class="our_services">
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_html_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div>
                <h3>PSD to HTML/CSS</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
            <div class="our_services">
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_email_template_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div>
                <h3>PSD to Email Template</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
            <div class="our_services">
                
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/psd_to_wordpress_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div> <!--  .service-box -->
                
                <h3>PSD to WordPress</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
            <div class="our_services">
                
                
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/wordpress_customization_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div> <!--  .service-box -->
                
                
                <h3>WordPress Customization</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
            <div class="our_services">
                
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/hubspot_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div> <!--  .service-box -->
                
                <h3>Hubspot Development</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
            <div class="our_services">
                
                <div class="service-box">
                    <div class="service-img-box">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="310.2px" height="240.4px" viewBox="0 -20 310.2 240.4" style="enable-background:new 0 0 310.2 220.4;" xml:space="preserve"
                            >
                        <g id="XMLID_1_">
                            <g id="XMLID_2_">
                                <circle id="XMLID_11_" class="wpb_bubble wpb_bubble1" cx="155" cy="50" r="2.6"/>
                                <circle id="XMLID_10_" class="wpb_bubble wpb_bubble2" cx="83" cy="203" r="2.8"/>
                                <circle id="XMLID_9_" class="wpb_bubble wpb_bubble3" cx="25" cy="97.5" r="4.6"/>
                                <circle id="XMLID_8_" class="wpb_bubble wpb_bubble4" cx="260" cy="30.3" r="3.1"/>
                                <circle id="XMLID_7_" class="wpb_bubble wpb_bubble5" cx="240" cy="145.1" r="2.6"/>
                                <circle id="XMLID_6_" class="wpb_bubble wpb_bubble6" cx="240" cy="192" r="1.5"/>
                                <circle id="XMLID_5_" class="wpb_bubble wpb_bubble7" cx="250" cy="81.3" r="1.5"/>
                                <circle id="XMLID_4_" class="wpb_bubble wpb_bubble8" cx="68.9" cy="72" r="2.8"/>
                                <circle id="XMLID_3_" class="wpb_bubble wpb_bubble9" cx="95" cy="155.7" r="2.8"/>
                            </g>
                            <path id="XMLID_17_" class="st0" d="M138.8,219.9c18.5-1,42.8-8.4,58.4-11.9c10.5-2.3,33.8-3.8,48.8-11.2
                                c3.5-1.7,36-28.2,10.6-66.2c-21.8-32.5,18.7-71.5,8.3-104.3c-9.9-31-42.1-19.4-55.8-9.3c-14.7,10.8-19.8,20.9-53.4,27.1
                                C119.2,50.9,38.3,71,25.3,85c-12.7,13.7-10.9,34,16,42.8s54.9,24.4,44.6,48.2s-13.5,37.9,8.4,41.6S128,220.5,138.8,219.9z"/>
                        </g>
                        </svg>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/deco_network_icon.svg" alt="">
                    </div> <!--  .service-img-box -->
                </div> <!--  .service-box -->
                
                <h3>DecoNetwork</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
            </div>
        </div>
        <div class="achivement_lists">
            <div class="container_wpb">
                <ul>
                    <li>
                        <h5>500+</h5>
                        <p>Projects</p>
                    </li>
                    <li>
                        <h5>8</h5>
                        <p>Plugins</p>
                    </li>
                    <li>
                        <h5>95k+</h5>
                        <p>Active Installs</p>
                    </li>
                    <li>
                        <h5>300k+</h5>
                        <p>Downloads</p>
                    </li>
                    <li>
                        <h5>10</h5>
                        <p>Team</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container_wpb">
            <div class="brigade_badges services_badges">
                <h3>Our WordPress Badges</h3>
                <ul>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/core_contributor.svg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/wordcamp_speaker.svg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/theme_review_team.svg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/theme_developer.svg" alt=""></a></li>



                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/plugin_peveloper.svg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/translation_editor.svg" alt=""></a></li>

                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/translation_contributor.svg" alt=""></a></li>
                </ul>
            </div>
            <div class="comments_section">
                <h3>Client Comments!</h3>
                <div class="comment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor.</p>
                </div>
                <div class="user_d">
                    <div class="user_d_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/services-1.png">
                    </div>
                    
                    <div class="user_inf">
                        <p>Zaryab Ahmed</p>
                        <a href="#">Zaryabkhan425@gmail.com</a>
                    </div>
                </div>
            </div>
        </div>
        <section class="hire_us_section quote" style="background-color: #0aa9eb">
            <div class="container_wpb1">
                <h4 class="h2">Do you need <br>The Best WordPress development services?</h4>
                <a href="#" class="quote_btn">Get a free Quote</a>
            </div>
        </section>

<?php
//get_sidebar();
get_footer("services");
?>