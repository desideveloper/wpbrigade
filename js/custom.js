( function( $ ) {
	$('.more-features').hide();

	$('.elementor-button-link[href="#features"]').on('click', function(x){
		x.preventDefault();
		var el=$(this).children('span').children('i');
		$('.more-features').slideToggle();
	});
} )( jQuery );
