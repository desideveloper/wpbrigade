<?php
/**
 * Template Name: Pricing2
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>

<div class="plugin_top">
	<div class="container_wpb clearfix">
		<div class="left_sec">
			
			<h2>BLOGGERZ</h2>
			<h3>Premium PSD to WordPress Development Agency</h3>
			<div class="live-demo">
				<a href="#" class="d_free">Download Free Version</a>
				
				<a href="#" class="buy_n">Buy Now</a>
				<p>Live demo</p>
			</div>
			
		</div>
		<div class="right_sec">
			<img src="<?php echo get_template_directory_uri(); ?>/img/mac.png">
		</div>
	</div>
</div>
<div class="achivement_lists plugin_achive">
    <div class="container_wpb">
        <ul>
            <li>
            	<p>Downloads</p>
                <h5>10k</h5>
            </li>
            <li>
            	<p>Version</p>
                <h5>1.1</h5>
            </li>
        </ul>
    </div>
</div>
<div class="plugin_main container_wpb">
	
	<div class="plugin_feature">
		<h4>Feature</h4>
		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_1.png">
			</div>
			<h3>SEO Friendly</h3>
			<p>Search Engine Optimization is a must these days if you want your business to have a strong online presence. Hestia Pro comes with an SEO-ready structure.</p>
		</div>

		<div class="feature  right">
			<div class="feature_icon">	
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_2.png">
			</div>
			<h3>Translation & RTL Ready</h3>
			<p>So you’re building a website that might not be written in English? No problem, Hestia Pro has you covered.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_3.png">
			</div>
			<h3>1-Minute Setup & Demo Content Available</h3>
			<p>Long story short, what this basically means is that, with Hestia Pro, you can get your new website installed and online in under 1 minute.</p>
		</div>

		<div class="feature  right">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_4.png">
			</div>
			<h3>Live Customizer</h3>
			<p>Fine-tuning your website has never been easier. With Hestia Pro, you can customize the way your website looks, and see the changes in real time.</p>
		</div>

		<div class="feature">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_5.png">
			</div>
			<h3>Video Tutorials</h3>
			<p>Want someone to guide you by the hand through the entire process of building your website with WordPress? We have video tutorials for that.</p>
		</div>

		<div class="feature right">
			<div class="feature_icon">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_6.png">
			</div>
			<h3>Mega Menus</h3>
			<p>Hestia Pro comes with a powerful, responsive, and user-friendly mega menu, making it easy to arrange and organize your submenu content.</p>
		</div>
	</div>
	
	<div class="pricing">
			<h4>Pricing</h4>
			<div class="buy_now clearfix">
				<div class="buy_left left">
					<h2>PSD to HTML</h2>
					<p>Lorem ipsum dolor sit onsectetur</p>
				</div>
				<div class="buy_right right">
					<h2>$49</h2>
					
				</div>
			</div>


			<div class="buy_now clearfix">
				<div class="buy_left left">
					<h2>PSD to Email Template</h2>
					<p>Lorem ipsum dolor sit onsectetur</p>
					
				</div>
				<div class="buy_right right">
					<h2>$49</h2>
					
				</div>
			</div>

			<div class="buy_now clearfix">
				<div class="buy_left left">
					<h2>PSD to WordPress</h2>
					<p>Lorem ipsum dolor sit onsectetur</p>
					
				</div>
				<div class="buy_right right">
					<h2>$49</h2>
					
				</div>
			</div>
		</div> 
	<div class="pricing">
		<h4>Theme Detail</h4>
		<div class="th_details">
			<h3>Created:</h3>
			<p>February 10, 2017</p>

			<h3>Last Update:</h3>
			<p>March 24, 2017</p>

			<h3>Version:</h3>
			<p>1.0.2 Changelog</p>

			<h3>How to use:</h3>
			<p>Documentation</p>

			<h3>Catagory:</h3>
			<p>Blog - Creative - Event - Landing page - Magazine - Music - Photography - Popular - Portfolio - Responsive</p>
		</div>
	</div>
</div>

<div class="faq">
	<div class="container_wpb">
		<h2>Frequently Asked Questions</h2>
		<div class="left">
			<h3>How can I purchase the theme?</h3>
			<p>You can purchase the theme using PayPal. It’s not necessary to have an account, just a credit or debit card to process the payment with.</p>

			<h3>How can I get updates?</h3>
			<p>With xMag Plus you get updates and theme downloads valid for one year, then renew at 30% off. You can update your theme using the traditional WordPress update tools or you can log in to your Design Lab account (or <a href="#">create a new one</a>) to download the latest update available.</p>

		</div>
		<div class="right">
			<h3>Can I use xMag Plus on more than one site?</h3>
			<p>Yes, all of our themes can be used on as many websites as you like, including personal websites or client/business sites.</p>

			<h3>Do I get support?</h3>
			<p>Our Themes include on year of support for common issues. Support for the customization is not included.<br>
			If you have questions about xMag Plus Theme you find <a href="#">documentation here</a> or you can <a href="#">open a help request here</a> (you must be a logged in member).</p>
		</div>
		<p class="hesitate">Do you have any question ? don't hesitate to <a href="#">Contact Us</a></p>
	</div>
</div>
<div class="plugin_testimonial">
	<h2>Testimonial</h2>
	<p class="some">some of our following comments.</p>
	<div class="container_wpb">
		<div class="comments_section right">
		    
		    <div class="comment">
		        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor.</p>
		    </div>
		    <div class="user_d">
		        <div class="user_d_img">
		            <img src="<?php echo get_template_directory_uri(); ?>/img/services-1.png">
		        </div>
		        
		        <div class="user_inf">
		            <p>Zaryab Ahmed</p>
		            <a href="#">Zaryabkhan425@gmail.com</a>
		        </div>
		    </div>
		</div>

		<div class="comments_section left">
		    <div class="comment">
		        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor.</p>
		    </div>
		    <div class="user_d">
		        <div class="user_d_img">
		            <img src="<?php echo get_template_directory_uri(); ?>/img/services-1.png">
		        </div>
		        
		        <div class="user_inf">
		            <p>Zaryab Ahmed</p>
		            <a href="#">Zaryabkhan425@gmail.com</a>
		        </div>
		    </div>
		</div>
	</div>
</div>











<?php
//get_sidebar();
get_footer();
?>