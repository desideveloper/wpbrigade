<?php
/**
 * Template Name: Plugin Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("home"); ?>
        <section class="plugins_title">
            <div class="container_wpb">
                <h1>Best Plugin Build by Team WPBrigade</h4>
                <p class="h3">We have crafted these innovative WordPress Plugins so far and more are in the process</p>
            </div>
        </section>
        <section class="plugins_list">
            <div class="container_wpb">
                <div class="badges_board">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/related-posts.png" alt="">
                    <ul class="stats">
                        <li class="icon-user">30,000+</li>
                        <li class="icon-downld">290,439</li>
                        <li class="icon-eye">4.9</li>
                    </ul> <!--  .stats -->
                    <span class="top_bar"></span>
                </div>
                <div class="plugin_detials">
                    <h3>Related Posts Thumbnails</h3>
                    <p>Related Posts is The Best Customizable plugin, that nicely displays related posts thumbnails under the post. Which increases user engagement with your blog.</p>
                    <a href="/wordpress/plugins/related-posts/" class="btn blue_btn">Read More</a>
                </div>
            </div>
        </section>
        <section class="plugins_list">
            <div class="container_wpb">
                <div class="badges_board">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/analytify.png" alt="">
                    <ul class="stats">
                        <li class="icon-user">10,000+</li>
                        <li class="icon-downld">91,982</li>
                        <li class="icon-eye">4.9</li>
                    </ul> <!--  .stats -->
                    <span class="top_bar"></span>
                </div>
                <div class="plugin_detials">
                    <h3>Google Analytics for WordPress</h3>
                    <p>Analytify is the must-have Plugin for Google Analytics in WordPress. It helps in tracking and reporting Google analytics events within WordPress.</p>
                    <a href="http://wpbrigade.com/recommend/analytify" target="_blank" class="btn blue_btn">Read More</a>
                </div>
            </div>
        </section>
        <section class="plugins_list">
            <div class="container_wpb">
                <div class="badges_board">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/login_press.png" alt="">
                    <ul class="stats">
                        <li class="icon-user">8,000+</li>
                        <li class="icon-downld">41,715</li>
                        <li class="icon-eye">5.0</li>
                    </ul> <!--  .stats -->
                    <span class="top_bar"></span>
                </div>
                <div class="plugin_detials">
                    <h3>LoginPress - Login Page Customizer</h3>
                    <p>LoginPress plugin allows you to easily customize the layout of login, admin login, client login, register and forgot password forms/pages.</p>
                    <a href="/wordpress/plugins/loginpress/" class="btn blue_btn">Read More</a>
                </div>
            </div>
        </section>

        <section class="plugins_list">
            <div class="container_wpb">
                <div class="badges_board">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/analytify.png" alt="">
                    <ul class="stats">
                        <li class="icon-user">50,000+</li>
                        <li class="icon-downld">369,184</li>
                        <li class="icon-eye">4.0</li>
                    </ul> <!--  .stats -->
                    <span class="top_bar"></span>
                </div>
                <div class="plugin_detials">
                    <h3>Simple Social Buttons</h3>
                    <p>Insert and customize social buttons: Facebook Like it, Share, LinkedIn, Google plus +1, Twitter share, Pinterest Pin it. Share your content with friends.</p>
                    <a href="http://wpbrigade.com/recommend/analytify" target="_blank" class="btn blue_btn">Read More</a>
                </div>
            </div>
        </section>
        <section class="hire_us_section" style="background-color: #fefefe">
            <div class="container_wpb">
                <h4 class="h2">Do you need The Best WordPress development services?</h4>
                <p>You are at the right place. Hire the Best WordPress Developers, Front-end Developmenter, CMS Development, Support and Maintenance</p>
                <a href="#" class="btn blue_btn hire_btn">Get a free Quote</a>
            </div>
        </section>

<?php
//get_sidebar();
get_footer();
?>